module github.com/SantiagoBedoya/golang-gin

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/go-playground/validator/v10 v10.4.1 // indirect
)
